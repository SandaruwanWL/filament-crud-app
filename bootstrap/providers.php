<?php

return [
    // Register the application's service providers
    App\Providers\AppServiceProvider::class, // Default application service provider
    App\Providers\Filament\AdminPanelProvider::class, // Filament admin panel service provider
];
