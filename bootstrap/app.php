<?php

use Illuminate\Foundation\Application;
use Illuminate\Foundation\Configuration\Exceptions;
use Illuminate\Foundation\Configuration\Middleware;

// Configure the Laravel application
return Application::configure(basePath: dirname(__DIR__))
    // Set the routing configuration
    ->withRouting(
        api: __DIR__.'/../routes/api.php', // API routes
        web: __DIR__.'/../routes/web.php', // Web routes
        commands: __DIR__.'/../routes/console.php', // Console commands
        health: '/up' // Health check endpoint
    )
    // Set the middleware configuration
    ->withMiddleware(function (Middleware $middleware) {
        // You can add middleware here if needed
    })
    // Set the exception handling configuration
    ->withExceptions(function (Exceptions $exceptions) {
        // You can add custom exception handling here if needed
    })
    // Create the application instance
    ->create();
