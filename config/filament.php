<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Filament Admin Panel Configuration
    |--------------------------------------------------------------------------
    |
    | This option allows you to configure the settings for the Filament admin
    | panel. You can customize the panel's appearance, authentication, and
    | other features to suit your application's needs.
    |
    */

    // The base URL path for the admin panel.
    'path' => 'admin',

    'auth' => [
        'guard' => 'web', // The guard used for authentication.
        'pages' => [
            'login' => \Filament\Http\Livewire\Auth\Login::class, // Default Filament login page.
        ],
    ],

    'middleware' => [
        'auth' => [
            \Illuminate\Auth\Middleware\Authenticate::class, // Use Laravel's default Authenticate middleware.
        ],
    ],

    'navigation' => [
        'group' => true, // Group navigation items by default.
    ],

    // The primary color for the admin panel.
    'primary_color' => 'indigo',

    'resources' => [
        'path' => app_path('Filament/Resources'), // Path to the Filament resources.
        'namespace' => 'App\\Filament\\Resources', // Namespace for the Filament resources.
    ],

    'pages' => [
        'path' => app_path('Filament/Pages'), // Path to the Filament pages.
        'namespace' => 'App\\Filament\\Pages', // Namespace for the Filament pages.
    ],

    'widgets' => [
        'path' => app_path('Filament/Widgets'), // Path to the Filament widgets.
        'namespace' => 'App\\Filament\\Widgets', // Namespace for the Filament widgets.
    ],
    
];

