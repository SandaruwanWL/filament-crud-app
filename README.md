# Filament CRUD App

This is a Filament CRUD application for managing customers and contacts. This README file contains all the steps and commands followed to create models, migrations, controllers, routes, resources, and more. It also explains how to use the app, UI placements, functionalities, error handling, and tests.

## Table of Contents

1. [Setup](#setup)
2. [Models](#models)
3. [Migrations](#migrations)
4. [Controllers](#controllers)
5. [Routes](#routes)
6. [Resources](#resources)
7. [UI Placements](#ui-placements)
8. [Functionalities](#functionalities)
9. [Error Handling](#error-handling)
10. [Tests](#tests)
11. [Widgets](#widgets)

## Setup

### Prerequisites

- PHP 8.2 or later
- Composer
- Node.js and npm
- MySQL or any other database supported by Laravel

### Installation

1. Clone the repository

   ```sh
   git clone git@gitlab.com:SandaruwanWL/filament-crud-app.git
   cd filament-crud-app
   ```

2. Install PHP dependencies

   ```sh
   composer install
   ```

3. Install JavaScript dependencies

   ```sh
   npm install
   ```

4. Copy `.env.example` to `.env` and configure your environment variables

   ```sh
   cp .env.example .env
   ```

5. Generate the application key

   ```sh
   php artisan key:generate
   ```

6. Create a MySQL database named "customer_crud_filament"

   ```sql
   CREATE DATABASE customer_crud_filament;
   ```

7. Configure your `.env` file to use the MySQL database

   ```env
   DB_CONNECTION=mysql
   DB_HOST=127.0.0.1
   DB_PORT=3306
   DB_DATABASE=customer_crud_filament
   DB_USERNAME=your_mysql_username
   DB_PASSWORD=your_mysql_password
   ```

8. Run migrations

   ```sh
   php artisan migrate
   ```

9. Build assets

   ```sh
   npm run dev
   ```

## Models

- Customer
- Contact

## Migrations

### Create Customers Table

```sh
php artisan make:migration create_customers_table --create=customers
```

### Create Contacts Table

```sh
php artisan make:migration create_contacts_table --create=contacts
```

### Update Migrations with Indexes

```php
Schema::create('customers', function (Blueprint $table) {
    $table->id();
    $table->string('name')->index();
    $table->integer('age')->nullable()->index();
    $table->string('hometown')->nullable()->index();
    $table->timestamps();
});

Schema::create('contacts', function (Blueprint $table) {
    $table->id();
    $table->foreignId('customer_id')->constrained()->onDelete('cascade')->index();
    $table->string('email')->index();
    $table->string('phone')->nullable()->index();
    $table->timestamps();
});
```

## Controllers

### API Controllers

```sh
php artisan make:controller API/CustomerController --api
php artisan make:controller API/ContactController --api
```

### Web Controllers

```sh
php artisan make:controller CustomerController --resource
php artisan make:controller ContactController --resource
```

## Routes

### API Routes

```php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\CustomerController;
use App\Http\Controllers\API\ContactController;

// Customer API routes
Route::get('/customers', [CustomerController::class, 'index']);
Route::post('/customers', [CustomerController::class, 'store']);
Route::get('/customers/{id}', [CustomerController::class, 'show']);
Route::put('/customers/{id}', [CustomerController::class, 'update']);
Route::delete('/customers/{id}', [CustomerController::class, 'destroy']);

// Contact API routes
Route::get('/contacts', [ContactController::class, 'index']);
Route::post('/contacts', [ContactController::class, 'store']);
Route::get('/contacts/{id}', [ContactController::class, 'show']);
Route::put('/contacts/{id}', [ContactController::class, 'update']);
Route::delete('/contacts/{id}', [ContactController::class, 'destroy']);
```

### Web Routes

```php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\ContactController;

// Customer web routes
Route::resource('customers', CustomerController::class);

// Contact web routes
Route::resource('contacts', ContactController::class);
```

## Resources

### Eloquent Models

#### Customer

```sh
php artisan make:model Customer --factory --migration --controller --resource
```

#### Contact

```sh
php artisan make:model Contact --factory --migration --controller --resource
```

## UI Placements

- Navigation: `resources/views/layouts/navigation.blade.php`
- Header: `resources/views/layouts/header.blade.php`
- Main Content: `resources/views/layouts/app.blade.php`

## Functionalities

- CRUD operations for customers and contacts.
- Form validation for creating and updating customers and contacts.
- Error handling and displaying appropriate error messages.

## Error Handling

- Custom error pages located in `resources/views/errors/`
- Middleware for handling and logging errors.

## Tests

- PHPUnit tests located in `tests/Feature/`

### Commands to run tests

```sh
php artisan test
```

### Running specific tests

```sh
php artisan test --filter=CustomerTest
php artisan test --filter=ContactTest
```

## Widgets

- Custom widgets for displaying customer and contact data.
- Widgets are located in `app/Filament/Widgets/`
