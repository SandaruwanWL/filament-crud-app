<?php

namespace Tests\Feature;

use App\Models\Customer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CustomerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if a customer can be created.
     *
     * @return void
     */
    public function test_it_can_create_a_customer()
    {
        // Send a POST request to create a new customer
        $response = $this->post('/api/customers', [
            'name' => 'John Doe',
            'age' => 30,
            'hometown' => 'New York'
        ]);

        // Assert that the response status is 201 (Created)
        $response->assertStatus(201)
                 ->assertJson([
                     'message' => 'Customer added successfully',
                     'customer' => [
                         'name' => 'John Doe',
                         'age' => 30,
                         'hometown' => 'New York'
                     ]
                 ]);

        // Assert that the customer exists in the database
        $this->assertDatabaseHas('customers', [
            'name' => 'John Doe',
            'age' => 30,
            'hometown' => 'New York'
        ]);
    }

    /**
     * Test if a specific customer can be shown.
     *
     * @return void
     */
    public function test_it_can_show_a_customer()
    {
        // Create a new customer
        $customer = Customer::factory()->create();

        // Send a GET request to retrieve the customer
        $response = $this->get('/api/customers/' . $customer->id);

        // Assert that the response status is 200 (OK)
        $response->assertStatus(200)
                 ->assertJson([
                     'message' => 'Customer found',
                     'customer' => [
                         'id' => $customer->id,
                         'name' => $customer->name,
                         'age' => $customer->age,
                         'hometown' => $customer->hometown
                     ]
                 ]);
    }

    /**
     * Test if a customer can be updated.
     *
     * @return void
     */
    public function test_it_can_update_a_customer()
    {
        // Create a new customer
        $customer = Customer::factory()->create();

        // Send a PUT request to update the customer
        $response = $this->put('/api/customers/' . $customer->id, [
            'name' => 'Jane Doe',
            'age' => 25,
            'hometown' => 'Los Angeles'
        ]);

        // Assert that the response status is 200 (OK)
        $response->assertStatus(200)
                 ->assertJson([
                     'message' => 'Customer updated successfully',
                     'customer' => [
                         'name' => 'Jane Doe',
                         'age' => 25,
                         'hometown' => 'Los Angeles'
                     ]
                 ]);

        // Assert that the customer exists in the database with updated data
        $this->assertDatabaseHas('customers', [
            'id' => $customer->id,
            'name' => 'Jane Doe',
            'age' => 25,
            'hometown' => 'Los Angeles'
        ]);
    }

    /**
     * Test if a customer can be deleted.
     *
     * @return void
     */
    public function test_it_can_delete_a_customer()
    {
        // Create a new customer
        $customer = Customer::factory()->create();

        // Send a DELETE request to delete the customer
        $response = $this->delete('/api/customers/' . $customer->id);

        // Assert that the response status is 200 (OK)
        $response->assertStatus(200)
                 ->assertJson([
                     'message' => 'Customer deleted'
                 ]);

        // Assert that the customer no longer exists in the database
        $this->assertDatabaseMissing('customers', [
            'id' => $customer->id
        ]);
    }

    /**
     * Test if customers can be listed.
     *
     * @return void
     */
    public function test_it_can_list_customers()
    {
        // Create multiple customers
        $customers = Customer::factory()->count(5)->create();

        // Send a GET request to list all customers
        $response = $this->get('/api/customers');

        // Assert that the response status is 200 (OK)
        $response->assertStatus(200)
                 ->assertJsonCount(5) // Assert that there are 5 customers in the response
                 ->assertJson([
                     [
                         'name' => $customers[0]->name,
                         'age' => $customers[0]->age,
                         'hometown' => $customers[0]->hometown
                     ],
                     // Additional assertions can be added here
                 ]);
    }
}
