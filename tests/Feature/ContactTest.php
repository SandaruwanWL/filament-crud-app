<?php

namespace Tests\Feature;

use App\Models\Contact;
use App\Models\Customer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ContactTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if a contact can be created.
     *
     * @return void
     */
    public function test_it_can_create_a_contact()
    {
        // Create a new customer
        $customer = Customer::factory()->create();

        // Send a POST request to create a new contact
        $response = $this->post('/api/contacts', [
            'customer_id' => $customer->id,
            'email' => 'john.doe@example.com',
            'phone' => '1234567890'
        ]);

        // Assert that the response status is 201 (Created)
        $response->assertStatus(201)
                 ->assertJson([
                     'message' => 'Contact added successfully',
                     'contact' => [
                         'customer_id' => $customer->id,
                         'email' => 'john.doe@example.com',
                         'phone' => '1234567890'
                     ]
                 ]);

        // Assert that the contact exists in the database
        $this->assertDatabaseHas('contacts', [
            'customer_id' => $customer->id,
            'email' => 'john.doe@example.com',
            'phone' => '1234567890'
        ]);
    }

    /**
     * Test if a specific contact can be shown.
     *
     * @return void
     */
    public function test_it_can_show_a_contact()
    {
        // Create a new contact
        $contact = Contact::factory()->create();

        // Send a GET request to retrieve the contact
        $response = $this->get('/api/contacts/' . $contact->id);

        // Assert that the response status is 200 (OK)
        $response->assertStatus(200)
                 ->assertJson([
                     'message' => 'Contact found',
                     'contact' => [
                         'id' => $contact->id,
                         'customer_id' => $contact->customer_id,
                         'email' => $contact->email,
                         'phone' => $contact->phone
                     ]
                 ]);
    }

    /**
     * Test if a contact can be updated.
     *
     * @return void
     */
    public function test_it_can_update_a_contact()
    {
        // Create a new contact and customer
        $contact = Contact::factory()->create();
        $customer = Customer::factory()->create();

        // Send a PUT request to update the contact
        $response = $this->put('/api/contacts/' . $contact->id, [
            'customer_id' => $customer->id,
            'email' => 'jane.doe@example.com',
            'phone' => '0987654321'
        ]);

        // Assert that the response status is 200 (OK)
        $response->assertStatus(200)
                 ->assertJson([
                     'message' => 'Contact updated successfully',
                     'contact' => [
                         'customer_id' => $customer->id,
                         'email' => 'jane.doe@example.com',
                         'phone' => '0987654321'
                     ]
                 ]);

        // Assert that the contact exists in the database with updated data
        $this->assertDatabaseHas('contacts', [
            'id' => $contact->id,
            'customer_id' => $customer->id,
            'email' => 'jane.doe@example.com',
            'phone' => '0987654321'
        ]);
    }

    /**
     * Test if a contact can be deleted.
     *
     * @return void
     */
    public function test_it_can_delete_a_contact()
    {
        // Create a new contact
        $contact = Contact::factory()->create();

        // Send a DELETE request to delete the contact
        $response = $this->delete('/api/contacts/' . $contact->id);

        // Assert that the response status is 200 (OK)
        $response->assertStatus(200)
                 ->assertJson([
                     'message' => 'Contact deleted'
                 ]);

        // Assert that the contact no longer exists in the database
        $this->assertDatabaseMissing('contacts', [
            'id' => $contact->id
        ]);
    }

    /**
     * Test if contacts can be listed.
     *
     * @return void
     */
    public function test_it_can_list_contacts()
    {
        // Create multiple contacts
        $contacts = Contact::factory()->count(5)->create();

        // Send a GET request to list all contacts
        $response = $this->get('/api/contacts');

        // Assert that the response status is 200 (OK)
        $response->assertStatus(200)
                 ->assertJsonCount(5) // Assert that there are 5 contacts in the response
                 ->assertJson([
                     [
                         'customer_id' => $contacts[0]->customer_id,
                         'email' => $contacts[0]->email,
                         'phone' => $contacts[0]->phone
                     ],
                     // Additional assertions can be added here
                 ]);
    }
}
