<div class="bg-white shadow rounded-lg p-6">
    <h3 class="text-lg font-medium text-gray-900 dark:text-gray-100">Recent Customers</h3>
    <ul>
        @foreach ($customers as $customer)
            <li class="py-2">
                <p class="text-sm text-gray-600 dark:text-gray-400">{{ $customer->name }}</p>
                <p class="text-xs text-gray-500 dark:text-gray-500">{{ $customer->created_at->diffForHumans() }}</p>
            </li>
        @endforeach
    </ul>
</div>
