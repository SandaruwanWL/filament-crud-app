<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\ContactController;

// Group the routes that require authentication middleware.
Route::middleware(['auth'])->group(function () {
    // Define resource routes for the CustomerController.
    // This will create routes for index, create, store, show, edit, update, and destroy actions.
    Route::resource('customers', CustomerController::class);

    // Define resource routes for the ContactController.
    // This will create routes for index, create, store, show, edit, update, and destroy actions.
    Route::resource('contacts', ContactController::class);
});

// Include additional authentication routes from the auth.php file.
require __DIR__.'/auth.php';
