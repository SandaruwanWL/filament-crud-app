<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\CustomerController;
use App\Http\Controllers\API\ContactController;

// Customer API routes
// Route to get a list of all customers.
Route::get('/customers', [CustomerController::class, 'index']);

// Route to create a new customer.
Route::post('/customers', [CustomerController::class, 'store']);

// Route to get a specific customer by ID.
Route::get('/customers/{id}', [CustomerController::class, 'show']);

// Route to update a specific customer by ID.
Route::put('/customers/{id}', [CustomerController::class, 'update']);

// Route to delete a specific customer by ID.
Route::delete('/customers/{id}', [CustomerController::class, 'destroy']);

// Contact API routes
// Route to get a list of all contacts.
Route::get('/contacts', [ContactController::class, 'index']);

// Route to create a new contact.
Route::post('/contacts', [ContactController::class, 'store']);

// Route to get a specific contact by ID.
Route::get('/contacts/{id}', [ContactController::class, 'show']);

// Route to update a specific contact by ID.
Route::put('/contacts/{id}', [ContactController::class, 'update']);

// Route to delete a specific contact by ID.
Route::delete('/contacts/{id}', [ContactController::class, 'destroy']);
