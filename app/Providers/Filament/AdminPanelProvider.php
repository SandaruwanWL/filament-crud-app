<?php

namespace App\Providers\Filament;

use App\Filament\Widgets\CustomStatsOverview; // Import the CustomStatsOverview widget
use App\Filament\Widgets\RecentCustomers; // Import the RecentCustomers widget
use App\Filament\Widgets\RecentContacts; // Import the RecentContacts widget
use Filament\Http\Middleware\Authenticate; // Import the Filament Authenticate middleware
use Filament\Http\Middleware\DisableBladeIconComponents; // Import the Filament middleware to disable Blade icon components
use Filament\Http\Middleware\DispatchServingFilamentEvent; // Import the Filament middleware to dispatch the serving Filament event
use Filament\Pages; // Import Filament pages
use Filament\Panel; // Import the Filament Panel class
use Filament\PanelProvider; // Import the Filament PanelProvider class
use Filament\Support\Colors\Color; // Import the Filament Color class for color support
use Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse; // Import the middleware to add queued cookies to the response
use Illuminate\Cookie\Middleware\EncryptCookies; // Import the middleware to encrypt cookies
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken; // Import the middleware to verify CSRF tokens
use Illuminate\Routing\Middleware\SubstituteBindings; // Import the middleware to substitute route model bindings
use Illuminate\Session\Middleware\AuthenticateSession; // Import the middleware to authenticate sessions
use Illuminate\Session\Middleware\StartSession; // Import the middleware to start sessions
use Illuminate\View\Middleware\ShareErrorsFromSession; // Import the middleware to share errors from the session
use App\Filament\Resources\CustomerResource; // Import the CustomerResource
use App\Filament\Resources\ContactResource; // Import the ContactResource

class AdminPanelProvider extends PanelProvider
{
    /**
     * Define the panel configuration.
     *
     * @param Panel $panel
     * @return Panel
     */
    public function panel(Panel $panel): Panel
    {
        return $panel
            ->default() // Use the default panel configuration
            ->id('admin') // Set the ID for the panel
            ->path('admin') // Set the base URL path for the admin panel
            ->login() // Enable the login page
            ->colors([
                'primary' => Color::Purple, // Set the primary color to purple
            ])
            ->resources([
                CustomerResource::class, // Register the CustomerResource
                ContactResource::class, // Register the ContactResource
            ])
            ->pages([
                Pages\Dashboard::class, // Register the Dashboard page
            ])
            ->widgets([
                // Widgets\AccountWidget::class, // Commented out: Account widget
                CustomStatsOverview::class, // Register the CustomStatsOverview widget
                RecentCustomers::class, // Register the RecentCustomers widget
                RecentContacts::class, // Register the RecentContacts widget
                // Widgets\FilamentInfoWidget::class, // Commented out: Filament Info widget
            ])
            ->middleware([
                EncryptCookies::class, // Encrypt cookies
                AddQueuedCookiesToResponse::class, // Add queued cookies to the response
                StartSession::class, // Start the session
                AuthenticateSession::class, // Authenticate the session
                ShareErrorsFromSession::class, // Share errors from the session
                VerifyCsrfToken::class, // Verify CSRF tokens
                SubstituteBindings::class, // Substitute route model bindings
                DisableBladeIconComponents::class, // Disable Blade icon components
                DispatchServingFilamentEvent::class, // Dispatch the serving Filament event
            ])
            ->authMiddleware([
                Authenticate::class, // Use the Filament Authenticate middleware
            ]);
    }
}
