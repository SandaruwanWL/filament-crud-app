<?php

namespace App\Filament\Pages;

use Filament\Pages\Dashboard as BaseDashboard;
use App\Filament\Widgets\CustomStatsOverview;
use App\Filament\Widgets\RecentCustomers; // Assuming you have this widget
use App\Filament\Widgets\RecentContacts; // Assuming you have this widget

class Dashboard extends BaseDashboard
{
    /**
     * Get the widgets for the dashboard.
     *
     * @return array
     */
    public function getWidgets(): array
    {
        return [
            CustomStatsOverview::class,
            RecentCustomers::class,
            RecentContacts::class,
        ];
    }

    /**
     * Define the number of columns for the dashboard layout.
     *
     * @return int
     */
    public function getColumns(): int
    {
        return 2; // Number of columns in the dashboard
    }
}

