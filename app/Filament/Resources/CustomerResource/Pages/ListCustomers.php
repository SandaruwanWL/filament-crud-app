<?php

namespace App\Filament\Resources\CustomerResource\Pages;

use App\Filament\Resources\CustomerResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListCustomers extends ListRecords
{
    // Specifies the resource that this page is associated with.
    protected static string $resource = CustomerResource::class;

    /**
     * Define the actions available in the header of the page.
     *
     * This method returns an array of actions, including the create action.
     *
     * @return array
     */
    protected function getHeaderActions(): array
    {
        return [
            // Adds a create action to the header actions.
            Actions\CreateAction::make(),
        ];
    }
}
