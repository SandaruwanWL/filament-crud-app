<?php

namespace App\Filament\Resources\CustomerResource\Pages;

use App\Filament\Resources\CustomerResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;
use Filament\Notifications\Notification;

class EditCustomer extends EditRecord
{
    // Specifies the resource that this page is associated with.
    protected static string $resource = CustomerResource::class;

    /**
     * Define the actions available in the header of the page.
     *
     * This method returns an array of actions, including the delete action.
     *
     * @return array
     */
    protected function getHeaderActions(): array
    {
        return [
            // Adds a delete action to the header actions.
            Actions\DeleteAction::make()
                ->action(function () {
                    // Delete the current record.
                    $this->record->delete();
                    // Show a success notification.
                    Notification::make()
                        ->title('Customer Deleted')
                        ->success()
                        ->send();
                    // Redirect to the customer index page.
                    $this->redirect($this->getResource()::getUrl('index'));
                }),
        ];
    }

    /**
     * Hook that is called after a customer is updated.
     */
    protected function afterSave(): void
    {
        // Custom notification after updating a customer.
        Notification::make()
            ->title('Customer Updated')
            ->success()
            ->send();

        // Redirect to the customer index page after saving.
        $this->redirect($this->getResource()::getUrl('index'));
    }

    /**
     * Override the default updated notification message.
     *
     * @return string|null
     */
    protected function getSavedNotificationMessage(): ?string
    {
        // Return empty string to prevent the default "Saved" notification.
        return "";
    }
}
