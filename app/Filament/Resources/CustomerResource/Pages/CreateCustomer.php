<?php

namespace App\Filament\Resources\CustomerResource\Pages;

use App\Filament\Resources\CustomerResource;
use Filament\Resources\Pages\CreateRecord;
use Filament\Notifications\Notification;

class CreateCustomer extends CreateRecord
{
    // Specifies the resource that this page is associated with.
    protected static string $resource = CustomerResource::class;

    /**
     * Get the URL to redirect to after creating a customer.
     *
     * @return string
     */
    protected function getRedirectUrl(): string
    {
        return $this->getResource()::getUrl('index'); // Redirect to the customer index page.
    }

    /**
     * Hook that is called after a customer is created.
     */
    protected function afterCreate(): void
    {
        // Custom notification after creating a customer.
        Notification::make()
            ->title('Customer added')
            ->success()
            ->send();
    }

    /**
     * Override the default created notification message.
     *
     * @return string|null
     */
    protected function getCreatedNotificationMessage(): ?string
    {
        // Return empty string to prevent the default "Created" notification.
        return "";
    }
}
