<?php

namespace App\Filament\Resources\ContactResource\Pages;

use App\Filament\Resources\ContactResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListContacts extends ListRecords
{
    // Specifies the resource that this page is associated with.
    protected static string $resource = ContactResource::class;

    /**
     * Define the actions available in the header of the page.
     *
     * This method returns an array of actions, including the create action.
     *
     * @return array
     */
    protected function getHeaderActions(): array
    {
        return [
            // Adds a create action to the header actions.
            Actions\CreateAction::make(),
        ];
    }
}
