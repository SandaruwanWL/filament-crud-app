<?php

namespace App\Filament\Resources\ContactResource\Pages;

use App\Filament\Resources\ContactResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;
use Filament\Notifications\Notification;

class EditContact extends EditRecord
{
    // Specifies the resource that this page is associated with.
    protected static string $resource = ContactResource::class;

    /**
     * Define the actions available in the header of the page.
     *
     * This method returns an array of actions, including the delete action.
     *
     * @return array
     */
    protected function getHeaderActions(): array
    {
        return [
            // Adds a delete action to the header actions.
            Actions\DeleteAction::make()
                ->action(function () {
                    $this->record->delete(); // Deletes the current record.
                    Notification::make()
                        ->title('Contact Deleted') // Custom notification for deletion.
                        ->success()
                        ->send();
                    $this->redirect($this->getResource()::getUrl('index')); // Redirects to the index page.
                }),
        ];
    }

    /**
     * Hook that is called after a contact is updated.
     */
    protected function afterSave(): void
    {
        // Custom notification after updating a contact.
        Notification::make()
            ->title('Contact Updated')
            ->success()
            ->send();

        // Redirect to the index page after saving.
        $this->redirect($this->getResource()::getUrl('index'));
    }

    /**
     * Override the default updated notification message.
     *
     * @return string|null
     */
    protected function getSavedNotificationMessage(): ?string
    {
        // Return empty string to prevent the default "Saved" notification.
        return "";
    }
}
