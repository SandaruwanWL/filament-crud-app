<?php

namespace App\Filament\Resources\ContactResource\Pages;

use App\Filament\Resources\ContactResource;
use Filament\Resources\Pages\CreateRecord;
use Filament\Notifications\Notification;

class CreateContact extends CreateRecord
{
    // Specifies the resource that this page is associated with.
    protected static string $resource = ContactResource::class;

    /**
     * Redirect URL after creating a contact.
     *
     * @return string
     */
    protected function getRedirectUrl(): string
    {
        // Redirect to the index page of the resource after creating a contact.
        return $this->getResource()::getUrl('index');
    }

    /**
     * Hook that is called after a contact is created.
     */
    protected function afterCreate(): void
    {
        // Custom notification after creating a contact.
        Notification::make()
            ->title('Contact Added')
            ->success()
            ->send();
    }

    /**
     * Override the default created notification message.
     *
     * @return string|null
     */
    protected function getCreatedNotificationMessage(): ?string
    {
        // Return empty string to prevent the default "Created" notification.
        return "";
    }
}
