<?php

namespace App\Filament\Resources;

use App\Filament\Resources\ContactResource\Pages;
use App\Models\Contact;
use Filament\Resources\Resource;
use Filament\Forms;
use Filament\Tables;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Select;
use Filament\Tables\Columns\TextColumn;

class ContactResource extends Resource
{
    // Specifies the model that this resource is associated with.
    protected static ?string $model = Contact::class;

    // Specifies the icon to use for this resource in the navigation.
    protected static ?string $navigationIcon = 'heroicon-o-phone';

    /**
     * Defines the form schema for creating and editing records.
     *
     * @param Forms\Form $form
     * @return Forms\Form
     */
    public static function form(Forms\Form $form): Forms\Form
    {
        return $form
            ->schema([
                // Select input for associating the contact with a customer.
                Select::make('customer_id')
                    ->label('Customer')
                    ->relationship('customer', 'name')
                    ->required(),
                // Text input for the contact's email.
                TextInput::make('email')
                    ->label('Email')
                    ->email()
                    ->required()
                    ->maxLength(255)
                    ->placeholder('Enter contact email')
                    ->helperText('Enter a valid email address'),
                // Text input for the contact's phone number.
                TextInput::make('phone')
                    ->label('Phone number')
                    ->tel()
                    ->required()
                    ->unique(Contact::class, 'phone', ignoreRecord: true)
                    ->maxLength(20)
                    ->placeholder('Enter contact phone number'),
            ]);
    }

    /**
     * Defines the table schema for displaying records.
     *
     * @param Tables\Table $table
     * @return Tables\Table
     */
    public static function table(Tables\Table $table): Tables\Table
    {
        return $table
            ->columns([
                // Column for displaying the associated customer's name.
                TextColumn::make('customer.name')
                    ->label('Customer')
                    ->sortable()
                    ->searchable(),
                // Column for displaying the contact's email.
                TextColumn::make('email')
                    ->label('Email')
                    ->sortable()
                    ->searchable(),
                // Column for displaying the contact's phone number.
                TextColumn::make('phone')
                    ->label('Phone')
                    ->sortable()
                    ->searchable(),
                // Column for displaying the creation date of the contact.
                TextColumn::make('created_at')
                    ->label('Created At')
                    ->dateTime(),
            ])
            ->filters([
                // Define any filters here.
            ]);
    }

    /**
     * Defines the pages available for this resource.
     *
     * @return array
     */
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListContacts::route('/'), // Route for listing contacts.
            'create' => Pages\CreateContact::route('/create'), // Route for creating a new contact.
            'edit' => Pages\EditContact::route('/{record}/edit'), // Route for editing a contact.
        ];
    }

    /**
     * Gets the badge count for the navigation item.
     *
     * @return string|null
     */
    public static function getNavigationBadge(): ?string
    {
        /** @var class-string<Model> $modelClass */
        $modelClass = static::$model;

        // Return the count of contact records as a string.
        return (string) $modelClass::count();
    }
}
