<?php

namespace App\Filament\Resources;

use App\Filament\Resources\CustomerResource\Pages;
use App\Models\Customer;
use Filament\Resources\Resource;
use Filament\Forms;
use Filament\Tables;
use Filament\Forms\Components\TextInput;
use Filament\Tables\Columns\TextColumn;

class CustomerResource extends Resource
{
    // Specifies the model that this resource is associated with.
    protected static ?string $model = Customer::class;

    // Specifies the icon to use for this resource in the navigation.
    protected static ?string $navigationIcon = 'heroicon-o-user';

    /**
     * Defines the form schema for creating and editing records.
     *
     * @param Forms\Form $form
     * @return Forms\Form
     */
    public static function form(Forms\Form $form): Forms\Form
    {
        return $form
            ->schema([
                // Text input for the customer's name.
                TextInput::make('name')
                    ->label('Name')
                    ->required()
                    ->placeholder('Enter customer name')
                    ->maxLength(255)
                    ->rules(['regex:/^[a-zA-Z\s]*$/'])
                    ->helperText('Name should only contain letters and spaces.'),
                // Text input for the customer's age.
                TextInput::make('age')
                    ->label('Age')
                    ->numeric()
                    ->required()
                    ->placeholder('Enter customer age')
                    ->minValue(0),
                // Text input for the customer's hometown.
                TextInput::make('hometown')
                    ->label('Hometown')
                    ->required()
                    ->placeholder('Enter customer hometown')
                    ->maxLength(255),
            ]);
    }

    /**
     * Defines the table schema for displaying records.
     *
     * @param Tables\Table $table
     * @return Tables\Table
     */
    public static function table(Tables\Table $table): Tables\Table
    {
        return $table
            ->columns([
                // Column for displaying the customer's name.
                TextColumn::make('name')
                    ->label('Name')
                    ->sortable()
                    ->searchable(),
                // Column for displaying the customer's age.
                TextColumn::make('age')
                    ->label('Age')
                    ->sortable()
                    ->searchable(),
                // Column for displaying the customer's hometown.
                TextColumn::make('hometown')
                    ->label('Hometown')
                    ->sortable()
                    ->searchable(),
                // Column for displaying the creation date of the customer.
                TextColumn::make('created_at')
                    ->label('Created At')
                    ->dateTime(),
            ])
            ->filters([
                // Define any filters here.
            ]);
    }

    /**
     * Defines the pages available for this resource.
     *
     * @return array
     */
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListCustomers::route('/'), // Route for listing customers.
            'create' => Pages\CreateCustomer::route('/create'), // Route for creating a new customer.
            'edit' => Pages\EditCustomer::route('/{record}/edit'), // Route for editing a customer.
        ];
    }

    /**
     * Gets the badge count for the navigation item.
     *
     * @return string|null
     */
    public static function getNavigationBadge(): ?string
    {
        /** @var class-string<Model> $modelClass */
        $modelClass = static::$model;

        // Return the count of customer records as a string.
        return (string) $modelClass::count();
    }
}
