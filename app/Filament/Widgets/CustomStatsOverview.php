<?php

namespace App\Filament\Widgets;

use Filament\Widgets\StatsOverviewWidget as BaseWidget;
use Filament\Widgets\StatsOverviewWidget\Card;

class CustomStatsOverview extends BaseWidget
{
    /**
     * Defines the cards to be displayed in the stats overview widget.
     *
     * @return array
     */
    protected function getCards(): array
    {
        // Get the current date in 'Y/m/d' format
        $currentDate = date('Y/m/d');

        // Get the current default timezone
        $timezone = date_default_timezone_get();

        // Return an array of Card instances with relevant data
        return [
            // Card to display total number of customers
            Card::make('Total Customers', \App\Models\Customer::count())
                ->description('This is the total number of customers') // Description for the card
                ->descriptionIcon('heroicon-o-users') // Icon for the description
                ->color('success'), // Color of the card

            // Card to display total number of contacts
            Card::make('Total Contacts', \App\Models\Contact::count())
                ->description('This is the total number of contacts') // Description for the card
                ->descriptionIcon('heroicon-o-phone') // Icon for the description
                ->color('primary'), // Color of the card

            // Card to display the current date and timezone
            Card::make('Current Date and Time', "{$currentDate} ({$timezone})")
                ->description('This is the current date and timezone') // Description for the card
                ->descriptionIcon('heroicon-o-clock') // Icon for the description
                ->color('info'), // Color of the card
        ];
    }
}
