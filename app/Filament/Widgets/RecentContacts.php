<?php

namespace App\Filament\Widgets;

use Filament\Widgets\Widget; // Import the base Widget class from Filament
use App\Models\Contact; // Import the Contact model

class RecentContacts extends Widget
{
    // Specifies the view file for this widget
    protected static string $view = 'filament.widgets.recent-contacts';

    /**
     * Get the data to pass to the view.
     *
     * This method fetches the most recent 5 contacts from the database
     * and returns them in an array to be used in the view.
     *
     * @return array
     */
    protected function getViewData(): array
    {
        // Fetch the latest 5 contacts from the database
        $contacts = Contact::latest()->take(5)->get();

        // Return the contacts data in an associative array
        return [
            'contacts' => $contacts,
        ];
    }
}
