<?php

namespace App\Filament\Widgets;

use Filament\Widgets\Widget; // Import the base Widget class from Filament
use App\Models\Customer; // Import the Customer model

class RecentCustomers extends Widget
{
    // Specifies the view file for this widget
    protected static string $view = 'filament.widgets.recent-customers';

    /**
     * Get the data to pass to the view.
     *
     * This method fetches the most recent 5 customers from the database
     * and returns them in an array to be used in the view.
     *
     * @return array
     */
    protected function getViewData(): array
    {
        // Fetch the latest 5 customers from the database
        $customers = Customer::latest()->take(5)->get();

        // Return the customers data in an associative array
        return [
            'customers' => $customers,
        ];
    }
}
