<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the contacts.
     *
     * This method retrieves all contacts from the database
     * and returns the 'contacts.index' view with the contacts.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        // Retrieve all contacts with the associated customer from the database.
        $contacts = Contact::with('customer')->get();

        // Return the 'contacts.index' view with the contacts data.
        return view('contacts.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new contact.
     *
     * This method returns the 'contacts.create' view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // Return the 'contacts.create' view.
        return view('contacts.create');
    }

    /**
     * Store a newly created contact in the database.
     *
     * This method validates the request data and creates a new contact.
     * It then redirects to the contacts index page with a success message.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        // Validate the request data.
        $request->validate([
            'customer_id' => 'required|exists:customers,id', // Ensure customer_id exists in the customers table.
            'email' => 'required|email',                   // Validate email format.
            'phone' => 'nullable|max:20',                  // Phone number is optional and should be max 20 characters.
        ]);

        // Create a new contact with the validated data.
        Contact::create($request->all());

        // Redirect to the contacts index page with a success message.
        return redirect()->route('contacts.index')->with('success', 'Contact created successfully.');
    }

    /**
     * Display the specified contact.
     *
     * This method retrieves a specific contact and returns the 'contacts.show' view with the contact data.
     *
     * @param \App\Models\Contact $contact
     * @return \Illuminate\View\View
     */
    public function show(Contact $contact)
    {
        // Eager load the customer relationship.
        $contact->load('customer');

        // Return the 'contacts.show' view with the contact data.
        return view('contacts.show', compact('contact'));
    }

    /**
     * Show the form for editing the specified contact.
     *
     * This method retrieves a specific contact and returns the 'contacts.edit' view with the contact data.
     *
     * @param \App\Models\Contact $contact
     * @return \Illuminate\View\View
     */
    public function edit(Contact $contact)
    {
        // Return the 'contacts.edit' view with the contact data.
        return view('contacts.edit', compact('contact'));
    }

    /**
     * Update the specified contact in the database.
     *
     * This method validates the request data and updates the specified contact.
     * It then redirects to the contacts index page with a success message.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Contact $contact
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Contact $contact)
    {
        // Validate the request data.
        $request->validate([
            'customer_id' => 'required|exists:customers,id', // Ensure customer_id exists in the customers table.
            'email' => 'required|email',                   // Validate email format.
            'phone' => 'nullable|max:20',                  // Phone number is optional and should be max 20 characters.
        ]);

        // Update the contact with the validated data.
        $contact->update($request->all());

        // Redirect to the contacts index page with a success message.
        return redirect()->route('contacts.index')->with('success', 'Contact updated successfully.');
    }

    /**
     * Remove the specified contact from the database.
     *
     * This method deletes the specified contact and redirects to the contacts index page with a success message.
     *
     * @param \App\Models\Contact $contact
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Contact $contact)
    {
        // Delete the specified contact.
        $contact->delete();

        // Redirect to the contacts index page with a success message.
        return redirect()->route('contacts.index')->with('success', 'Contact deleted successfully.');
    }
}
