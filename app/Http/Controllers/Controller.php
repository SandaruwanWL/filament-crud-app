<?php

namespace App\Http\Controllers;

/**
 * Base controller class for the application.
 *
 * This abstract class can be extended by other controllers to inherit common functionality.
 */
abstract class Controller
{
    
}
