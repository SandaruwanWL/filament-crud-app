<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileUpdateRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class ProfileController extends Controller
{
    /**
     * Display the user's profile form.
     *
     * This method returns the 'profile.edit' view with the current user's data.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\View\View
     */
    public function edit(Request $request): View
    {
        // Return the 'profile.edit' view with the current user's data.
        return view('profile.edit', [
            'user' => $request->user(),
        ]);
    }

    /**
     * Update the user's profile information.
     *
     * This method validates the request data using the ProfileUpdateRequest,
     * updates the user's profile, and redirects back to the profile edit page with a success message.
     *
     * @param \App\Http\Requests\ProfileUpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProfileUpdateRequest $request): RedirectResponse
    {
        // Fill the user model with the validated data from the request.
        $request->user()->fill($request->validated());

        // If the email field was changed, set email_verified_at to null.
        if ($request->user()->isDirty('email')) {
            $request->user()->email_verified_at = null;
        }

        // Save the updated user model.
        $request->user()->save();

        // Redirect back to the profile edit page with a success message.
        return Redirect::route('profile.edit')->with('status', 'profile-updated');
    }

    /**
     * Delete the user's account.
     *
     * This method validates the request for the user's password, logs out the user,
     * deletes the user account, invalidates the session, regenerates the session token,
     * and redirects to the home page.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request): RedirectResponse
    {
        // Validate the request for the user's current password.
        $request->validateWithBag('userDeletion', [
            'password' => ['required', 'current_password'],
        ]);

        // Get the current user.
        $user = $request->user();

        // Log out the user.
        Auth::logout();

        // Delete the user account.
        $user->delete();

        // Invalidate the current session.
        $request->session()->invalidate();

        // Regenerate the session token.
        $request->session()->regenerateToken();

        // Redirect to the home page.
        return Redirect::to('/admin');
    }
}
