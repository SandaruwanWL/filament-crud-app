<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the contacts.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        // Retrieve all contacts with the associated customer from the database.
        $contacts = Contact::with('customer')->get();

        // Return the contacts as a JSON response with a 200 OK status.
        return response()->json($contacts, 200);
    }

    /**
     * Store a newly created contact in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        // Validate the incoming request data.
        $validatedData = $request->validate([
            'customer_id' => 'required|exists:customers,id',
            'email' => 'required|email|max:255',
            'phone' => 'required|string|max:20|unique:contacts,phone',
        ]);

        // Create a new contact using the validated data.
        $contact = Contact::create($validatedData);

        // Return the created contact data along with a custom message and a 201 Created status.
        return response()->json([
            'message' => 'Contact added successfully',
            'contact' => $contact
        ], 201);
    }

    /**
     * Display the specified contact.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        // Find the contact by its ID.
        $contact = Contact::with('customer')->find($id);

        // Check if the contact exists.
        if (!$contact) {
            // Return a 404 error response if the contact is not found.
            return response()->json([
                'message' => 'Contact not found'
            ], 404);
        }

        // Return the contact data along with a custom message and a 200 OK status.
        return response()->json([
            'message' => 'Contact found',
            'contact' => $contact
        ], 200);
    }

    /**
     * Update the specified contact in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        // Validate the incoming request data.
        $validatedData = $request->validate([
            'customer_id' => 'required|exists:customers,id',
            'email' => 'required|email|max:255',
            'phone' => 'required|string|max:20|unique:contacts,phone,' . $id,
        ]);

        // Find the contact by its ID.
        $contact = Contact::findOrFail($id);

        // Update the contact using the validated data.
        $contact->update($validatedData);

        // Return the updated contact data along with a custom message and a 200 OK status.
        return response()->json([
            'message' => 'Contact updated successfully',
            'contact' => $contact
        ], 200);
    }

    /**
     * Remove the specified contact from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        // Find the contact by its ID.
        $contact = Contact::findOrFail($id);

        // Delete the contact from the database.
        $contact->delete();

        // Return a success message with a 200 OK status.
        return response()->json(['message' => 'Contact deleted'], 200);
    }
}
