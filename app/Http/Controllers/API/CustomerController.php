<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the customers.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        // Retrieve all customers with their related contacts and return them as a JSON response with a 200 OK status.
        $customers = Customer::with('contacts')->get();
        return response()->json($customers, 200);
    }

    /**
     * Store a newly created customer in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        // Create a new customer using all data from the request.
        $customer = Customer::create($request->all());

        // Load the related contacts to apply eager loading.
        $customer->load('contacts');

        // Return the created customer data along with a custom message and a 201 Created status.
        return response()->json([
            'message' => 'Customer added successfully',
            'customer' => $customer
        ], 201);
    }

    /**
     * Display the specified customer.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        // Find the customer by ID with its related contacts.
        $customer = Customer::with('contacts')->find($id);

        // If the customer is not found, return a 404 Not Found response with a custom message.
        if (!$customer) {
            return response()->json([
                'message' => 'Customer not found'
            ], 404);
        }

        // If the customer is found, return the customer data along with a custom message and a 200 OK status.
        return response()->json([
            'message' => 'Customer found',
            'customer' => $customer
        ], 200);
    }

    /**
     * Update the specified customer in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        // Validate the incoming request data.
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'age' => 'required|integer',
            'hometown' => 'required|string|max:255',
        ]);

        // Find the customer by ID or fail with a 404 Not Found response.
        $customer = Customer::findOrFail($id);

        // Update the customer with the validated data.
        $customer->update($validatedData);

        // Load the related contacts to apply eager loading.
        $customer->load('contacts');

        // Return the updated customer data along with a custom message and a 200 OK status.
        return response()->json([
            'message' => 'Customer updated successfully',
            'customer' => $customer
        ], 200);
    }

    /**
     * Remove the specified customer from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        // Find the customer by ID or fail with a 404 Not Found response.
        $customer = Customer::findOrFail($id);

        // Delete the customer.
        $customer->delete();

        // Return a custom message with a 200 OK status.
        return response()->json(['message' => 'Customer deleted'], 200);
    }
}
