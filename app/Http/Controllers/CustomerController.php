<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the customers.
     *
     * This method retrieves all customers from the database with their related contacts
     * and returns the 'customers.index' view with the customers.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        // Retrieve all customers with their related contacts from the database.
        $customers = Customer::with('contacts')->get();

        // Return the 'customers.index' view with the customers data.
        return view('customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new customer.
     *
     * This method returns the 'customers.create' view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // Return the 'customers.create' view.
        return view('customers.create');
    }

    /**
     * Store a newly created customer in the database.
     *
     * This method validates the request data and creates a new customer.
     * It then redirects to the customers index page with a success message.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        // Validate the request data.
        $request->validate([
            'name' => ['required', 'max:255', 'regex:/^[a-zA-Z\s]*$/'], // Validate name: required and max length 255.
            'email' => 'required|email|unique:customers,email', // Validate email: required, valid format, and unique in the customers table.
            'phone' => 'nullable|max:20',                  // Phone number is optional and should be max 20 characters.
        ]);

        // Create a new customer with the validated data.
        Customer::create($request->all());

        // Redirect to the customers index page with a success message.
        return redirect()->route('customers.index')->with('success', 'Customer created successfully.');
    }

    /**
     * Display the specified customer.
     *
     * This method retrieves a specific customer with their related contacts
     * and returns the 'customers.show' view with the customer data.
     *
     * @param \App\Models\Customer $customer
     * @return \Illuminate\View\View
     */
    public function show(Customer $customer)
    {
        // Load the related contacts for the customer.
        $customer->load('contacts');

        // Return the 'customers.show' view with the customer data.
        return view('customers.show', compact('customer'));
    }

    /**
     * Show the form for editing the specified customer.
     *
     * This method retrieves a specific customer and returns the 'customers.edit' view with the customer data.
     *
     * @param \App\Models\Customer $customer
     * @return \Illuminate\View\View
     */
    public function edit(Customer $customer)
    {
        // Return the 'customers.edit' view with the customer data.
        return view('customers.edit', compact('customer'));
    }

    /**
     * Update the specified customer in the database.
     *
     * This method validates the request data and updates the specified customer.
     * It then redirects to the customers index page with a success message.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Customer $customer
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Customer $customer)
    {
        // Validate the request data.
        $request->validate([
            'name' => ['required', 'max:255', 'regex:/^[a-zA-Z\s]*$/'], // Validate name: required and max length 255.
            'email' => 'required|email|unique:customers,email,' . $customer->id, // Validate email: required, valid format, and unique in the customers table, ignoring the current customer's ID.
            'phone' => 'nullable|max:20',                          // Phone number is optional and should be max 20 characters.
        ]);

        // Update the customer with the validated data.
        $customer->update($request->all());

        // Redirect to the customers index page with a success message.
        return redirect()->route('customers.index')->with('success', 'Customer updated successfully.');
    }

    /**
     * Remove the specified customer from the database.
     *
     * This method deletes the specified customer and redirects to the customers index page with a success message.
     *
     * @param \App\Models\Customer $customer
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Customer $customer)
    {
        // Delete the specified customer.
        $customer->delete();

        // Redirect to the customers index page with a success message.
        return redirect()->route('customers.index')->with('success', 'Customer deleted successfully.');
    }
}
