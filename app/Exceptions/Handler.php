<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * These exceptions are not logged in the application's logs.
     *
     * @var array
     */
    protected $dontReport = [
        // Add exceptions here that you don't want to report
    ];

    /**
     * Report or log an exception.
     *
     * This method is used to log exceptions or send them to an external service.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        // Call the parent report method to ensure the exception is reported.
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * This method converts exceptions into HTTP responses that can be sent to the client.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $exception)
    {
        // Handle validation exceptions.
        if ($exception instanceof ValidationException) {
            return response()->json([
                'error' => 'Validation Error',
                'messages' => $exception->errors(),
            ], 422); // Return a JSON response with status code 422.
        }

        // Handle authentication exceptions.
        if ($exception instanceof AuthenticationException) {
            return response()->json(['error' => 'Unauthenticated'], 401); // Return a JSON response with status code 401.
        }

        // Call the parent render method for other exceptions.
        return parent::render($request, $exception);
    }
}

