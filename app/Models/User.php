<?php

namespace App\Models;

// Import necessary classes and traits for the User model.
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    // Use the HasFactory and Notifiable traits for factory and notification features.
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * This array defines which attributes can be mass-assigned.
     * Mass-assignment means assigning values to multiple attributes at once,
     * usually via an array. For example, when creating or updating a model.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',     // The name of the user.
        'email',    // The email address of the user.
        'password', // The password of the user.
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * This array defines which attributes should be hidden
     * when the model is converted to an array or JSON.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',        // The password should be hidden.
        'remember_token',  // The remember token should be hidden.
    ];

    /**
     * Get the attributes that should be cast.
     *
     * This method returns an array of attributes that should be cast to
     * specific types. Casting helps to convert attributes to a specific type
     * when retrieving them from the database.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'email_verified_at' => 'datetime', // Cast the email_verified_at attribute to a datetime.
            'password' => 'hashed',            // Hash the password attribute.
        ];
    }
}
