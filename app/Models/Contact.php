<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    // Use the HasFactory trait to enable factory features for this model.
    use HasFactory;

    // Specify the relationships that should be eager loaded by default.
    protected $with = ['customer'];

    /**
     * The attributes that are mass assignable.
     *
     * This array defines which attributes can be mass-assigned.
     * Mass-assignment means assigning values to multiple attributes at once,
     * usually via an array. For example, when creating or updating a model.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'customer_id', // The ID of the associated customer.
        'email',       // The email address of the contact.
        'phone',       // The phone number of the contact.
    ];

    /**
     * Get the customer that owns the contact.
     *
     * This method defines an inverse one-to-many relationship.
     * It indicates that each contact belongs to one customer.
     * The belongsTo method is used to define this relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        // Define the inverse relationship with the Customer model.
        return $this->belongsTo(Customer::class);
    }
}
