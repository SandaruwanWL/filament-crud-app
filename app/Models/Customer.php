<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    // Use the HasFactory trait to enable factory features for this model.
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * This array defines which attributes can be mass-assigned.
     * Mass-assignment means assigning values to multiple attributes at once,
     * usually via an array. For example, when creating or updating a model.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',     // The name of the customer.
        'age',      // The age of the customer.
        'hometown', // The hometown of the customer.
    ];

    /**
     * Get the contacts for the customer.
     *
     * This method defines a one-to-many relationship.
     * It indicates that each customer can have multiple contacts.
     * The hasMany method is used to define this relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contacts()
    {
        // Define a one-to-many relationship with the Contact model.
        return $this->hasMany(Contact::class);
    }

    /**
     * Scope a query to include related contacts.
     *
     * This scope method adds eager loading for the contacts relationship.
     * It can be used to ensure that contacts are always loaded with customers.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithContacts($query)
    {
        return $query->with('contacts');
    }
}
