<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\View\View;

/**
 * GuestLayout component class.
 *
 * This component renders the guest layout view.
 */
class GuestLayout extends Component
{
    /**
     * Get the view / contents that represents the component.
     *
     * @return \Illuminate\View\View
     */
    public function render(): View
    {
        // Return the view for the guest layout.
        return view('layouts.guest');
    }
}
