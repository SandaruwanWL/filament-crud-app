<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\View\View;

/**
 * AppLayout component class.
 *
 * This component renders the application's main layout view.
 */
class AppLayout extends Component
{
    /**
     * Get the view / contents that represents the component.
     *
     * @return \Illuminate\View\View
     */
    public function render(): View
    {
        // Return the view for the application's main layout.
        return view('layouts.app');
    }
}
