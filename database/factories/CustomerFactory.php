<?php

namespace Database\Factories;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Factories\Factory;

class CustomerFactory extends Factory
{
    // Specify the name of the model that this factory is for.
    protected $model = Customer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            // Generate a fake name.
            'name' => $this->faker->name,

            // Generate a fake age between 18 and 80.
            'age' => $this->faker->numberBetween(18, 80),

            // Generate a fake city as hometown.
            'hometown' => $this->faker->city,
        ];
    }
}
