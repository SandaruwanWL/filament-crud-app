<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Create the 'cache' table.
        Schema::create('cache', function (Blueprint $table) {
            $table->string('key')->primary(); // Primary key is the cache key.
            $table->mediumText('value'); // Cache value.
            $table->integer('expiration')->index(); // Cache expiration time, indexed for faster lookups.
        });

        // Create the 'cache_locks' table.
        Schema::create('cache_locks', function (Blueprint $table) {
            $table->string('key')->primary(); // Primary key is the lock key.
            $table->string('owner'); // Owner of the lock.
            $table->integer('expiration')->index(); // Lock expiration time, indexed for faster lookups.
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        // Drop the 'cache' table.
        Schema::dropIfExists('cache');

        // Drop the 'cache_locks' table.
        Schema::dropIfExists('cache_locks');
    }
};
