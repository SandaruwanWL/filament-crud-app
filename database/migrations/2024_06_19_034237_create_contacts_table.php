<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * This method is used to create the 'contacts' table in the database
     * with the specified columns and their data types.
     *
     * @return void
     */
    public function up()
    {
        // Create the 'contacts' table.
        Schema::create('contacts', function (Blueprint $table) {
            $table->id(); // Auto-incrementing primary key.
            $table->foreignId('customer_id') // Foreign key referencing 'customers' table.
                ->constrained()              // References the 'id' column on the 'customers' table.
                ->onDelete('cascade')        // Deletes contact if the related customer is deleted.
                ->index();                   // Index for the customer_id column.
            $table->string('email')->index(); // Contact email column of type string with an index.
            $table->string('phone')->nullable()->index(); // Contact phone column of type string, nullable, with an index.
            $table->timestamps();            // Timestamp columns: created_at and updated_at.
        });
    }

    /**
     * Reverse the migrations.
     *
     * This method is used to drop the 'contacts' table
     * if the migration is rolled back.
     *
     * @return void
     */
    public function down()
    {
        // Drop the 'contacts' table.
        Schema::dropIfExists('contacts');
    }
}
