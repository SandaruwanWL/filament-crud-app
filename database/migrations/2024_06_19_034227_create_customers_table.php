<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * This method is used to create the 'customers' table in the database
     * with the specified columns and their data types.
     *
     * @return void
     */
    public function up()
    {
        // Create the 'customers' table.
        Schema::create('customers', function (Blueprint $table) {
            $table->id();                  // Auto-incrementing primary key.
            $table->string('name')->index(); // Customer name column of type string with an index.
            $table->integer('age')->nullable()->index(); // Customer age column of type integer, nullable, with an index.
            $table->string('hometown')->nullable()->index(); // Customer hometown column of type string, nullable, with an index.
            $table->timestamps();          // Timestamp columns: created_at and updated_at.
        });
    }

    /**
     * Reverse the migrations.
     *
     * This method is used to drop the 'customers' table
     * if the migration is rolled back.
     *
     * @return void
     */
    public function down()
    {
        // Drop the 'customers' table.
        Schema::dropIfExists('customers');
    }
}
