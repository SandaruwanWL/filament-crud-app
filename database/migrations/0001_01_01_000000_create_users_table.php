<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Create the 'users' table.
        Schema::create('users', function (Blueprint $table) {
            $table->id(); // Auto-incrementing primary key.
            $table->string('name'); // User name.
            $table->string('email')->unique(); // User email, must be unique.
            $table->timestamp('email_verified_at')->nullable(); // Email verification timestamp.
            $table->string('password'); // User password.
            $table->rememberToken(); // Token for "remember me" functionality.
            $table->timestamps(); // Timestamps for created_at and updated_at.
        });

        // Create the 'password_reset_tokens' table.
        Schema::create('password_reset_tokens', function (Blueprint $table) {
            $table->string('email')->primary(); // Primary key is the email.
            $table->string('token'); // Token for password reset.
            $table->timestamp('created_at')->nullable(); // Timestamp for token creation.
        });

        // Create the 'sessions' table.
        Schema::create('sessions', function (Blueprint $table) {
            $table->string('id')->primary(); // Primary key is the session ID.
            $table->foreignId('user_id')->nullable()->index(); // Foreign key for user ID, indexed for faster lookups.
            $table->string('ip_address', 45)->nullable(); // IP address of the session, supports both IPv4 and IPv6.
            $table->text('user_agent')->nullable(); // User agent string.
            $table->longText('payload'); // Session payload.
            $table->integer('last_activity')->index(); // Timestamp of the last activity, indexed for faster lookups.
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        // Drop the 'users' table.
        Schema::dropIfExists('users');

        // Drop the 'password_reset_tokens' table.
        Schema::dropIfExists('password_reset_tokens');

        // Drop the 'sessions' table.
        Schema::dropIfExists('sessions');
    }
};
