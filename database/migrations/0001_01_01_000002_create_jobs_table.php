<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Create the 'jobs' table.
        Schema::create('jobs', function (Blueprint $table) {
            $table->id(); // Auto-incrementing primary key.
            $table->string('queue')->index(); // Queue name, indexed for faster lookups.
            $table->longText('payload'); // Job payload.
            $table->unsignedTinyInteger('attempts'); // Number of attempts made to process the job.
            $table->unsignedInteger('reserved_at')->nullable()->index(); // Time when the job was reserved.
            $table->unsignedInteger('available_at')->index(); // Time when the job is available.
            $table->unsignedInteger('created_at')->index(); // Time when the job was created.
        });

        // Create the 'job_batches' table.
        Schema::create('job_batches', function (Blueprint $table) {
            $table->string('id')->primary(); // Primary key is the batch ID.
            $table->string('name'); // Batch name.
            $table->integer('total_jobs'); // Total number of jobs in the batch.
            $table->integer('pending_jobs'); // Number of pending jobs in the batch.
            $table->integer('failed_jobs'); // Number of failed jobs in the batch.
            $table->longText('failed_job_ids'); // IDs of failed jobs.
            $table->mediumText('options')->nullable(); // Batch options, nullable.
            $table->integer('cancelled_at')->nullable()->index(); // Time when the batch was cancelled.
            $table->integer('created_at')->index(); // Time when the batch was created.
            $table->integer('finished_at')->nullable()->index(); // Time when the batch was finished.
        });

        // Create the 'failed_jobs' table.
        Schema::create('failed_jobs', function (Blueprint $table) {
            $table->id(); // Auto-incrementing primary key.
            $table->string('uuid')->unique(); // Unique identifier for the failed job.
            $table->text('connection'); // Connection name.
            $table->text('queue'); // Queue name.
            $table->longText('payload'); // Job payload.
            $table->longText('exception'); // Exception message.
            $table->timestamp('failed_at')->useCurrent()->index(); // Time when the job failed, indexed.
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        // Drop the 'jobs' table.
        Schema::dropIfExists('jobs');

        // Drop the 'job_batches' table.
        Schema::dropIfExists('job_batches');

        // Drop the 'failed_jobs' table.
        Schema::dropIfExists('failed_jobs');
    }
};
